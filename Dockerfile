FROM gitlab-registry.cern.ch/cms-cactus/core/ts/v5-1-1
MAINTAINER Cactus Role Account <cactus@cern.ch>

ADD .gitlab-ci.docker/cactus-amc13.centos7.x86_64.repo /etc/yum.repos.d/
ADD .gitlab-ci.docker/cactus-mp7.centos7.x86_64.repo /etc/yum.repos.d/

RUN \
	yum install -y \
		cactusboards-amc13-amc13-1.2.8 cactusboards-amc13-tools-1.2.8 \
		cactusboards-amc13-python-1.2.8 \
	 && \
	yum install -y \
		cactusboards-mp7-mp7-2.4.1 \
		cactusboards-mp7-pycomp7-2.4.1 \
		cactusboards-mp7-tests-2.4.1 \
	 && \
	yum clean all

ADD ci_rpms /ci_rpms
RUN \
	yum install -y /ci_rpms/*.rpm && \
	rm -rf /ci_rpms/ && \
	yum clean all

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["/bin/bash"]

#  vim: set ft=dockerfile noet :  
