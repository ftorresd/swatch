FROM gitlab-registry.cern.ch/cms-cactus/core/swatch/swatch-buildenv-665b4159a49d

MAINTAINER Cactus Role Account <cactus@cern.ch>

ARG SWATCH_VERSION
ARG BUILD_UTILS_VERSION=0.1.0

# http://cactus.web.cern.ch/cactus/release/swatch/latest_doc/html/usersGuide/install.html
ADD cactus-swatch.centos7.x86_64.repo /etc/yum.repos.d/
ADD cactus-build-utils.noarch.repo /etc/yum.repos.d/

RUN yum install -y cactuscore-swatch*-$SWATCH_VERSION cactuscore-build-utils-$BUILD_UTILS_VERSION

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["/bin/bash"]
