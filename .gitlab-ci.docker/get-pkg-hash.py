#!/usr/bin/env python
from __future__ import print_function
import hashlib
import itertools
import re
import sys

dockerfile = open(sys.argv[1],'r').read()
dockerfile = re.sub('\s*\\\s*\n\s*',' ',dockerfile)
packages = re.findall('yum (?:group)?install(?:\s+-\S+)*((?:\s+[a-zA-Z]\S*)+)',dockerfile)
packages = itertools.chain.from_iterable(map(lambda x: x.split(' '), packages))
packages = filter(None, map(lambda x: x.strip(), packages))
packages = sorted(packages)
print(hashlib.md5('\n'.join(packages)).hexdigest()[0:12])
